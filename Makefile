SHELL := /bin/bash

.DEFAULT: freshinstall
.PHONY: freshinstall
freshinstall:
	make stop
	make remove
	make prepare
	make start

.PHONY: prepare
prepare:
	echo "Makefile prepare placeholder"
	docker compose up --no-start

.PHONY: remove
remove:
	-docker container rm vue-docker
	-docker image rm vue-docker
	-@if [ -d "./node_modules" ]; then \
		read -p "Are you sure you wish to delete the existing package-lock.json and node_modules folder? (y/n)? " -n 1 -r; \
		echo ; \
		if [ "$$REPLY" = "y" ]; then \
			rm -rf ./node_modules; \
			rm package-lock.json; \
		fi \
	fi

.PHONY: start
start:
	( sleep 3; open http://localhost:3000 || xdg-open http://localhost:3000 || echo "Open 'http://localhost:3000' in a browser to view BrowserSync pages." ) &
	docker compose up

.PHONY: stop
stop:
	-docker compose down

.PHONY: restart
restart:
	make stop
	make start

.PHONY: sh
sh:
	docker exec -it vue-docker sh