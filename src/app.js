// import TestComponent from './TestComponent.vue'

const app = Vue.createApp({
    data() {
        return {
            title: "Title",
            author: "Author",
            age: 50
        }
    },
    methods: {
        changeTitle(title) {
            this.title = title
            // setInterval(() => {
            //     this.age++
            //   }, 1000)          
        }
    }
})

// app.component('test-component', TestComponent)

app.mount('#app')